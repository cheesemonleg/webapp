# Stage 1 build and install dependencies
FROM node:15-alpine3.13 as build

RUN apk add --no-cache python2 python3 make g++

# Install Node Dependencies
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm i

# Additional pre-req
RUN npm rebuild node-sass

# Stage 2 add files and remove unwanted files. Also generate the SHA
FROM node:15-alpine3.13 as preparse

WORKDIR /usr/src/app
RUN apk add --no-cache git

COPY --from=build /usr/src/app/node_modules ./node_modules
COPY . .

RUN git rev-parse --short HEAD > COMMITSHA

# Remove git directory
RUN rm -rf .git && rm -rf .gitignore && rm -rf Dockerfile && rm -rf .gitlab-ci.yml

# Stage 3 copy and prepare final image
FROM node:15-alpine3.13

WORKDIR /usr/src/app
COPY --from=preparse /usr/src/app .

EXPOSE 3000

CMD [ "npm", "start" ]

