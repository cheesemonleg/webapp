// %%*
// Anything between this quote will be read-only
// *%%

// %% This line is read-only

fun main() {
    println("Hello World!")
}