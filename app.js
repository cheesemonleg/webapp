const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const sassMiddleware = require('node-sass-middleware');
const favicon = require('serve-favicon');

const indexRouter = require('./routes/index');
const setupRouter = require('./routes/setup');
const userRouter = require('./routes/users');
const surveyRouter = require('./routes/survey');
const courseRouter = require('./routes/course');
const qbRouter = require('./routes/questions');
const codeEditorRouter = require('./routes/codeeditor');
const quizAttemptRouter = require('./routes/quizattempt');
const reportRouter = require('./routes/reports');
const healthRouter = require('./routes/health');

const redis = require('./lib/redis');
const session = require('express-session');
const redisStore = require('connect-redis')(session);
const appUtil = require('./lib/util');
const log = require('./lib/logger');
const appConf = require('./config');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
log.info(`[INIT] Initialized Redis Session Cache with a timeout expiry of ${redis.config.ttl} seconds`);
app.set('trust proxy', appConf.miscConfigs.trustProxy);
log.info(`[INIT] Trust Proxy: ${appConf.miscConfigs.trustProxy}`);
app.use(favicon(path.join(__dirname, 'public/favicon/favicon.ico')));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(sassMiddleware({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public'),
    indentedSyntax: true, // true = .sass and false = .scss
    sourceMap: true
}));
app.use(express.static(path.join(__dirname, 'public')));

// Init, setup permissions
log.info("Updating Permissions on Redis if exists"); // noinspection JSIgnoredPromiseFromCall
appUtil.forceRefreshPermission();

app.use('/health', healthRouter);

app.use(session({
    secret: redis.config.secret,
    name: '_redisSessionData',
    resave: false,
    saveUninitialized: true,
    cookie: {secure: false},
    store: new redisStore({
        host: redis.config.host,
        port: redis.config.port,
        password: redis.config.password,
        client: redis.client,
        ttl: redis.config.ttl
    })
}));
app.use('/', indexRouter);
app.use('/setup', setupRouter);
app.use('/users', userRouter);
app.use('/survey', surveyRouter);
app.use('/course', courseRouter);
app.use('/questions', qbRouter);
app.use('/editor', codeEditorRouter);
app.use('/attempt', quizAttemptRouter);
app.use('/reports', reportRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
    next(createError(404));
});

// error handler
app.use((err, req, res) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
