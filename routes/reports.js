const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const {miscConfigs} = require('../config');
const log = require('../lib/logger');

const cache = require('../lib/redis');
const db = require('../lib/db');
const moment = require('moment');
const humanizeDuration = require('humanize-duration');
const csv = require('fast-csv');
const appUtil = require('../lib/util');
const { ProxyAuthenticationRequired } = require('http-errors');

const renderConf = {appName: 'Reports - AASP', route: 'Results'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });

async function canViewQuizReport(res, quizid, redirect= true) {
    if (await auth.hasPermissionBool(renderConf, 'viewallreport')) return true;
    let quiz = (await db.query("SELECT courseid FROM quiz WHERE id=? AND deleted=0", quizid))[0][0];
    if (!quiz) { if (redirect) res.redirect(`/reports/list?action=np&id=${quizid}`); return false; } // If no permission to touch this quiz
    let [chk] = await db.query('SELECT * FROM course_users WHERE userid=? AND courseid=? AND instructor=1', [renderConf.loginId, quiz.courseid]);
    let course = (await db.query('SELECT * FROM courses WHERE id=? AND deleted=0', [quiz.courseid]))[0][0];
    if (!chk || chk.length <= 0 || !course) { if (redirect) res.redirect(`/reports/list?action=np&id=${quizid}`); return false; } // If no permission to touch this course
    return true;
}

// List quizzes where user can see reports
router.get('/list', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewquizreport', 'viewallreport');
    let errmsg;
    if (req.query.action) {
        switch (req.query.action) {
            case 'np': errmsg = "You do not have permission to modify this quiz"; break;
            case 'errp': errmsg = "Unable to find this quiz attempt"; break;
        }
    }
    let isadminLink = (req.query.admin && req.query.admin === 'true');
    let isadmin = (isadminLink) ? "/data/allQuiz" : "/data/managedQuiz";
    let newrc = {...renderConf};
    if (isadminLink) newrc.route = 'Admin';
    res.render('reports/viewreportquizlist', {...renderConf, errmsg: errmsg, title: 'View Reports', dataLink: isadmin, adminMode: isadminLink});
});

// List users that can take the quiz
router.get('/students/:quizid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewquizreport', 'viewallreport'); // Can view all reports
    let qid = req.params.quizid;
    if (!(await canViewQuizReport(res, qid))) return; // Can view report for this quiz

    let redirectLink = '/reports/list';
    if (req.query.dashboard) redirectLink = '/dashboard';

    let quiz = (await db.query("SELECT name, courseid FROM quiz WHERE id=? AND deleted=0", qid))[0][0];
    let course = (await db.query("SELECT code, name FROM courses WHERE id=?", quiz.courseid))[0][0];

    res.render('reports/viewquizstudents', {...renderConf, quizid: qid, redirectLink: redirectLink, quizData: quiz, courseData: course, title: `View Students (${quiz.name})`});
});

// Get data of students/instructors that can take the quiz and their current quiz taking attempt statistics
router.get('/students/:quizid/data', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewquizreport', 'viewallreport'); // Can view all reports
    let qid = req.params.quizid;
    if (!(await canViewQuizReport(res, qid, false))) { res.json({}); return; } // Check can view report for this quiz

    let quiz = (await db.query("SELECT courseid FROM quiz WHERE id=? AND deleted=0", qid))[0][0];
    let [quizStudents] = await db.query("SELECT cu.*, u.username, u.fullname, u.email, d.shortname, d.fullname as deptname FROM course_users cu JOIN users u on cu.userid = u.id JOIN departments d on u.department = d.id WHERE cu.courseid=?", quiz.courseid); // Get all students for this quiz
    let [quizAttempts] = await db.query("SELECT * FROM quiz_attempt WHERE quizid=?", qid); // Get all attempts for quiz

    let studentTempData = {}; // We are going to store max score and attempt count for student here
    for (let qa of quizAttempts) {
        if (qa.studentid in studentTempData) {
            // Existing student, calculate and increment attempt count
            let existingStudent = studentTempData[qa.studentid];
            existingStudent.attempt += 1;
            if (qa.status === 4 && qa.score !== null && qa.score > existingStudent.score) existingStudent.score = qa.score;
            studentTempData[qa.studentid] = existingStudent;
        } else {
            // New studewnt, create new attempt count
            let newStudent = {score: 0, attempt: 1};
            if (qa.status === 4 && qa.score !== null) newStudent.score = qa.score;
            studentTempData[qa.studentid] = newStudent;
        }
    }

    // Craft out datatables data file
    let data = [];
    for (let student of quizStudents) {
        let studentData = {userid: student.userid, instructor: (student.instructor === 1), username: student.username, name: student.fullname, email: student.email, deptShort: student.shortname, deptLong: student.deptname, highScore: 0, attempts: 0 }
        if (student.userid in studentTempData) {
            studentData.highScore = studentTempData[student.userid].score;
            studentData.attempts = studentTempData[student.userid].attempt;
        }
        data.push(studentData);
    }
    res.json({data: data});
});

// Show attempts made in a quiz by a user
router.get('/attempts/:quizid/:userid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewquizreport', 'viewallreport'); // Can view all reports
    let qid = req.params.quizid, uid = req.params.userid;
    if (!(await canViewQuizReport(res, qid))) return; // Can view report for this quiz

    let quiz = (await db.query("SELECT name, courseid FROM quiz WHERE id=? AND deleted=0", qid))[0][0];
    let course = (await db.query("SELECT code, name FROM courses WHERE id=?", quiz.courseid))[0][0];
    let user = (await db.query("SELECT fullname, username FROM users WHERE id=?", uid))[0][0]; // Get student name

    let errmsg, sucmsg;
    if (req.query.action) {
        switch (req.query.action) {
            case 'nm': errmsg = "This quiz attempt has not been marked by the system yet"; break;
            case 'dels': sucmsg = "Quiz Attempt deleted successfully!"; break;
            case 'rems': sucmsg = "Quiz Attempt has been sent for remarking"; break;
            case 'delf': errmsg = "An error occurred deleting quiz attempt"; break;
        }
    }

    res.render('reports/viewuserattempts', {...renderConf, quizid: qid, userid: uid, quizname: quiz.name, username: user.fullname,
        un: user.username, courseData: course, errmsg: errmsg, sucmsg: sucmsg, title: `View Attempts (${user.fullname}) - ${quiz.name}`});
});

function getStatus(status) {
    switch (status) {
        case 0: return "Generating Attempt";
        case 1: return "Attempt In Progress";
        case 2: return "Awaiting Marking";
        case 3: return "Marking";
        case 4: return "Marked";
        case 5: return "Error";
        default: return "Unknown";
    }
}

// Get data of all attempts made in a quiz by a user
router.get('/attempts/:quizid/:userid/data', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewquizreport', 'viewallreport'); // Can view all reports
    let qid = req.params.quizid, uid = req.params.userid;
    if (!(await canViewQuizReport(res, qid, false))) { res.json({}); return; } // Check can view report for this quiz

    let [quizAttempts] = await db.query("SELECT * FROM quiz_attempt WHERE quizid=? AND studentid=? ORDER BY date DESC", [qid, uid]); // Get all attempts for quiz

    // Craft out datatables data file
    let data = [], i=1;
    let best = -1, bestScore = -1, bestDate = -1;
    for (let attempt of quizAttempts) {
        let rec = {ind: i, createDate: attempt.date, rawStatus: attempt.status, score: attempt.score, markedDate: attempt.markeddate, submittedDate: attempt.submitteddate,
            attemptid: attempt.id, status: getStatus(attempt.status), best: false};
        data.push(rec);
        // Check if best or not
        if (attempt.score > bestScore || (attempt.score === bestScore && attempt.submitteddate > bestDate)) {
            // Better score overall, use this
            // Same score, but newer submitted date to use
            best = attempt.id;
            bestScore = attempt.score;
            bestDate = attempt.submitteddate;
        }
        i++;
    }
    log.info(`Best Attempt: ${best}`);
    // Set best attempt
    for (let d of data) {
        if (d.attemptid === best) {
            d.best = true;
        }
    }

    res.json({data: data});
});

async function getReportData(quizid, courseid, attemptid, res, override=false) {
    // Get some data out
    let quizdata = (await db.query("SELECT q.name, q.showgrade, q.studentreport, q.totalscore, q.timeend, c.code, c.name as coursename FROM quiz q JOIN courses c on q.courseid = c.id WHERE q.id=?", quizid))[0][0];
    if (quizdata.showgrade === 0 && quizdata.studentreport===0 && !override) {res.redirect(`/course/info/${courseid}/${quizid}/start?noreport=1`); return;}
    // Get questions from quiz and obtain max score
    let [quizqns] = await db.query("SELECT qq.id, qq.answer, qq.score, q.questionid, q2.question, q2.maxscore, q2.type FROM quiz_attempt_qn qq JOIN quiz_questions q on qq.questionid = q.id JOIN question q2 on q.questionid = q2.id WHERE qq.attemptid=?", attemptid);
    let maxscore = 0;
    for (let qn of quizqns) {
        maxscore += parseFloat(qn.maxscore);
        switch (qn.type) {
            case 1:
                //Implement MCQ based data
                if(qn.answer !== 'null'){
                    qn.choice = extractText(qn.answer);
                let mcqType = (await db.query("SELECT * FROM question_mcq WHERE questionid=?", qn.questionid))[0][0];
                qn.numOfSel = qn.choice.length;
                    if(mcqType.style == 0){
                        let [dbAns] = await db.query("SELECT * FROM question_mcq_ans WHERE questionid=?", qn.questionid);
                        qn.dbAns = [dbAns].answer;
                        var answerArr = [];
                        var rationaleArr = [];
                        var isCorrectArr = [];
                        for(let x of dbAns){
                            if((qn.choice.indexOf(x.answer) > -1)){
                                answerArr.push(x.answer);
                                isCorrectArr.push(x.iscorrect);
                                rationaleArr.push(x.rationale);
                            }else if(x.iscorrect){
                                answerArr.push(x.answer);
                                isCorrectArr.push(0);
                                rationaleArr.push(x.rationale);
                            }
                        }
                        qn.mcqAnswer = answerArr;
                        qn.mcqCorrect = isCorrectArr;
                        qn.mcqRationale = rationaleArr;
                        
                    }else if(mcqType.style == 1){
                        if(qn.choice != ""){
                            let dbAnswer = (await db.query("SELECT * FROM question_mcq_ans WHERE answer=?", qn.choice))[0][0];
                            qn.mcqAnswer = dbAnswer.answer;
                            qn.mcqCorrect = dbAnswer.iscorrect;
                            qn.mcqRationale = dbAnswer.rationale;
                        }else{
                            qn.mcqAnswer = "No Answer Submitted";
                            qn.mcqCorrect = "N/A";
                            qn.mcqRationale = "N/A";
                        }
                    }
                }
                break;
            case 2:
                // Implement structured based data
                let structData = (await db.query("SELECT * FROM question_structured qs JOIN quiz_questions qq on qs.questionid = qq.questionid JOIN quiz_attempt_qn qaq on qq.id = qaq.questionid WHERE qaq.id=?", qn.id))[0][0];
                qn.struct = structData;
                break;
            case 3:
                // Implement code based data
                let codeattemptdata = (await db.query("SELECT * FROM quiz_attempt_code WHERE attemptid=?", qn.id))[0][0];
                if (codeattemptdata && codeattemptdata !== 'undefined') {
                    qn.codelang = codeattemptdata.language;
                    qn.tests = {total: codeattemptdata.totaltests, pass: codeattemptdata.passedtests, fail: codeattemptdata.failedtests, error: codeattemptdata.erroredtests};
                } else {
                    qn.codelang = 'java';
                    qn.tests = {total: 0, pass: 0, fail: 0, error: 0};
                    qn.answer = "No Answer Submitted";
                }
                qn.codelangcss = getLangCss(qn.codelang);
                break;
        }
    }
    if (parseFloat(quizdata.totalscore) < 0) quizdata.totalscore = maxscore;

    return [quizdata, quizqns];
}

function extractText(str){
    var found = [],          // an array to collect the strings that are found
        rxp = /{([^}]+)}/g,
        str = str,
        curMatch;

    while( curMatch = rxp.exec( str ) ) {
        found.push( curMatch[1] );
    }

    return found;
}

function getLangCss(lang) {
    if (lang.startsWith('c-')) return 'c';
    else if (lang.startsWith('cpp-')) return 'cpp';
    else if (lang.startsWith('python-')) return 'python';
    else return lang;
}

// View report of a quiz attempt by a TA/Prof
router.get('/view/:quizattempt/reportTA', async (req, res) => {
    let quizattemptid = req.params.quizattempt;
    let studentchk = (await db.query("SELECT qa.quizid, qa.studentid, q.courseid, qa.status, qa.score, qa.markeddate, qa.submitteddate, qa.date AS attemptdate FROM quiz_attempt qa JOIN quiz q ON q.id = qa.quizid WHERE qa.id=?", quizattemptid))[0][0];
    if (!studentchk) {res.redirect(`/reports/list?action=errp`); return;}
    if (studentchk.status !== 4) {res.redirect(`/reports/attempts/${studentchk.quizid}/${studentchk.studentid}?action=nm`); return;}

    let [quizdata, quizqns] = await getReportData(studentchk.quizid, studentchk.courseid, quizattemptid, res, true);

    // Set view attempt to enabled
    quizdata.showGrade = true;
    quizdata.showReport = true;

    res.render('reports/reportdetail', {...renderConf, title: `Attempt Report - #${quizattemptid}`, appName: "AASP", info: studentchk, data: quizqns, qinfo: quizdata, aid: quizattemptid, backurl: `/reports/attempts/${studentchk.quizid}/${studentchk.studentid}`, backname: "Go Back"});
});

// View report of a quiz attempt by a student
router.get("/view/:quizattempt/reportSA", async (req, res) => {
    let quizattemptid = req.params.quizattempt;
    let studentchk = (await db.query("SELECT qa.quizid, qa.studentid, q.courseid, qa.status, qa.score, qa.markeddate, qa.submitteddate, qa.date AS attemptdate FROM quiz_attempt qa JOIN quiz q ON q.id = qa.quizid WHERE qa.id=?", quizattemptid))[0][0];
    if (!studentchk || typeof studentchk === 'undefined' || studentchk.studentid !== renderConf.loginId) {res.redirect(`/course/info/${studentchk.courseid}/${studentchk.quizid}/start?invaliduser=1`); return;}
    if (studentchk.status !== 4) {res.redirect(`/attempt/${quizattemptid}/getReportStudentAttempt`); return;}

    let [quizdata, quizqns] = await getReportData(studentchk.quizid, studentchk.courseid, quizattemptid, res);

    quizdata.showGrade = quizdata.showgrade === 1;
    quizdata.showReport = quizdata.studentreport === 1;

    res.render('reports/reportdetail', {...renderConf, title: `Attempt Report - #${quizattemptid}`, appName: "AASP", info: studentchk, data: quizqns, qinfo: quizdata, aid: quizattemptid, backurl: `/course/info/${studentchk.courseid}`, backname: "Back to Course"});
});

// Delete quiz attempt
router.get("/manage/:quizattempt/delete", async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewquizreport', 'viewallreport'); // Can view all reports
    let qaid = req.params.quizattempt
    let quizdata = (await db.query("SELECT quizid, studentid FROM quiz_attempt WHERE id=?", qaid))[0][0];
    if (!(await canViewQuizReport(res, quizdata.quizid))) return; // Can view report for this quiz

    // Delete
    let dbConn = await db.getConnection();
    try {
        await dbConn.beginTransaction();
        await dbConn.query("DELETE FROM quiz_attempt_code WHERE attemptid=?", qaid);
        await dbConn.query("DELETE FROM quiz_attempt_qn WHERE attemptid=?", qaid);
        await dbConn.query("DELETE FROM quiz_attempt WHERE id=?", qaid);
        await dbConn.commit();
        log.info("Deleted Quiz Atteempt #", qaid);
        res.redirect(`/reports/attempts/${quizdata.quizid}/${quizdata.studentid}?action=dels`);
    } catch (err) {
        await dbConn.rollback();
        log.error(err);
        res.redirect(`/reports/attempts/${quizdata.quizid}/${quizdata.studentid}?action=delf`);
    } finally {
        await dbConn.release();
    }
});

// Remark attempt
router.get("/manage/:quizattempt/remark", async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'viewquizreport', 'viewallreport'); // Can view all reports
    let qaid = req.params.quizattempt
    let quizdata = (await db.query("SELECT quizid, studentid FROM quiz_attempt WHERE id=?", qaid))[0][0];
    if (!(await canViewQuizReport(res, quizdata.quizid))) return; // Can view report for this quiz

    // Remark attempt
    await db.query("UPDATE quiz_attempt SET status=2, score=null, markeddate=null WHERE id=?", qaid); // Update state
    await cache.rpush(miscConfigs.submissionQueue, `s-${qaid}`); // Push to submissions
    res.redirect(`/reports/attempts/${quizdata.quizid}/${quizdata.studentid}?action=rems`);
});

// Students view past quizzes
router.get('/history', async (req, res) => {
    let errmsg;
    if (req.query.action) {
        switch (req.query.action) {
            case 'errp': errmsg = "Unable to find this quiz attempt"; break;
        }
    }
    res.render('reports/viewpastquizlist', {...renderConf, errmsg: errmsg, title: 'View Past Quizzes'});
});

// Get data for all past quizzes for the user
router.get('/history/data', async (req, res) => {
    let userid = renderConf.loginId;
    let [courses] = await db.query("SELECT c.*, cu.instructor FROM courses c JOIN course_users cu on c.id = cu.courseid WHERE cu.userid=? AND c.deleted=0", userid);
    let availQuizzes = [];
    for (let course of courses) {
        let isManagingCourse = course.instructor === 1;
        console.log(course.id);
        // Get all quizzes that either has no end time or whose end date is before now
        let [quizzes] = await db.query("SELECT * FROM quiz WHERE courseid=? AND deleted=0 AND timestart < CURRENT_TIMESTAMP AND (((studentreport = 2 OR showgrade = 2) AND (timeend IS NULL OR timeend <= CURRENT_TIMESTAMP)) OR (studentreport=1 OR showgrade=1))", course.id);
        for (let quiz of quizzes) {
            if (quiz.timelimit === -1) quiz.timelimit = "Unlimited";
            if (quiz.attempts === -1) quiz.attempts = "Unlimited";
            let [attempts] = await db.query("SELECT COUNT(id) as counter FROM quiz_attempt WHERE quizid=? AND studentid=?", [quiz.id, userid]);
            quiz.hasAttempt = attempts && attempts.length > 0 && attempts[0].counter > 0;
            if (!quiz.hasAttempt) continue; // Don't include as theres no results
            availQuizzes.push({courseid: course.id, quizid: quiz.id, quizname: quiz.name, start: quiz.timestart, end: quiz.timeend, description: quiz.description,
                coursename: course.name, coursecode: course.code, managedCourse: isManagingCourse});
        }
    }
    res.json({data: availQuizzes});
});

// Show attempts made in a quiz by the current user
router.get('/history/view/:quizid', async (req, res) => {
    let qid = req.params.quizid, uid = renderConf.loginId;

    let quiz = (await db.query("SELECT name FROM quiz WHERE id=?", qid))[0][0]; // Get quiz name
    let user = (await db.query("SELECT fullname FROM users WHERE id=?", uid))[0][0]; // Get student name

    let errmsg;
    if (req.query.action) {
        switch (req.query.action) {
            case 'ie': errmsg = "Attempt is still being marked"; break;
        }
    }

    res.render('reports/viewuserattemptsstudent', {...renderConf, quizid: qid, userid: uid, quizname: quiz.name, errmsg: errmsg,
        username: user.fullname, title: `View Attempts (${user.fullname}) - ${quiz.name}`});
});

// Get data of all attempts made in a quiz by the current user
router.get('/history/view/:quizid/data', async (req, res) => {
    let qid = req.params.quizid, uid = renderConf.loginId;

    // Get all attempts for quiz
    let [quizAttempts] = await db.query("SELECT * FROM quiz_attempt WHERE quizid=? AND studentid=? ORDER BY date DESC", [qid, uid]);

    // Craft out datatables data file
    let data = [], i=1;
    for (let attempt of quizAttempts) {
        let rec = {ind: i, createDate: attempt.date, rawStatus: attempt.status, score: attempt.score, markedDate: attempt.markeddate,
            submittedDate: attempt.submitteddate, attemptid: attempt.id, status: getStatus(attempt.status)};
        data.push(rec);
        i++;
    }
    res.json({data: data});
});

// View report of a quiz attempt by current user (end of quiz)
router.get("/view/:quizattempt/reportSE", async (req, res) => {
    let quizattemptid = req.params.quizattempt;
    let studentchk = (await db.query("SELECT qa.quizid, qa.studentid, q.courseid, qa.status, qa.score, qa.markeddate, qa.submitteddate, qa.date AS attemptdate FROM quiz_attempt qa JOIN quiz q ON q.id = qa.quizid WHERE qa.id=?", quizattemptid))[0][0];
    if (!studentchk || typeof studentchk === 'undefined' || studentchk.studentid !== renderConf.loginId) {res.redirect(`/reports/history?action=errp`); return;}
    if (studentchk.status !== 4) {res.redirect(`/reports/history/view/${studentchk.quizid}?action=ie`); return;}

    let [quizdata, quizqns] = await getReportData(studentchk.quizid, studentchk.courseid, quizattemptid, res);

    quizdata.showGrade = quizdata.showgrade === 1;
    quizdata.showReport = quizdata.studentreport === 1;

    // Check if quiz has expired. If so use the end settings
    if (quizdata.timeend != null && quizdata.timeend <= new Date()) {
        quizdata.showGrade = quizdata.showgrade > 0;
        quizdata.showReport = quizdata.studentreport > 0;
    }

    res.render('reports/reportdetail', {...renderConf, title: `Attempt Report - #${quizattemptid}`, appName: "AASP", info: studentchk, data: quizqns, qinfo: quizdata, aid: quizattemptid, backurl: `/reports/history/view/${studentchk.quizid}`, backname: "Back to Attempts"});
});

module.exports = router;
