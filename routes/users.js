const express = require('express');
const router = express.Router();
const auth = require('../lib/auth');
const appUtil = require('../lib/util');
const log = require('../lib/logger');

const db = require('../lib/db');
const cache = require('../lib/redis');
const csv = require('fast-csv');

const renderConf = {appName: 'User Management - AASP', route: 'Admin'};

router.use('*', async (req, res, next) => { if (await auth.initHeader(req, res, renderConf)) next(); });

// Add new user to app
router.get('/adduser', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adduser');
    let [departments] = await db.query('SELECT * FROM departments');
    let [roles] = await db.query('SELECT * FROM roles WHERE power < 9999'); // Do not show SA
    res.render('users/adduser', {...renderConf, title: 'Add New User', departments: departments, roles: roles});
});

router.get('/listfeedbacks', async (req, res) => {
    res.render('survey/listfeedbacks', {...renderConf, title: 'Feedbacks'});
});

// Add a new user to the app
router.post('/adduser', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adduser');
    let [departments] = await db.query('SELECT * FROM departments');
    let [roles] = await db.query('SELECT * FROM roles WHERE power < 9999'); // Do not show SA

    try {
        let username = await auth.createUser(req.body.username, req.body.password, req.body.fullName, req.body.email, req.body.userrole, req.body.department);
        res.redirect(`/users/list?addsuc=${username}`);
    } catch (err) {
        log.error(err);
        let errorMessage = err.message;
        if (err.code === 'ER_DUP_ENTRY') errorMessage = `Failed to add ${req.body.username} into the system. Username already exists`;
        res.render('users/adduser', {...renderConf, title: 'Add New User', departments: departments, roles: roles, errmsg: errorMessage})
    }
});

// Import users into app page
router.get('/import', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adduser');
    let extraData = {};
    if (req.query.fin) {
        let data = await cache.get(req.query.fin);
        data = JSON.parse(data);
        if (data.errors.length > 0) extraData.dataerror = data.errors;
        else extraData.datasuccess = true;
    }
    let [departments] = await db.query('SELECT * FROM departments');
    let [roles] = await db.query('SELECT * FROM roles WHERE power < 9999'); // Do not show SA
    res.render('users/importuser', {...renderConf, title: 'Import User', departments: departments, roles: roles, ...extraData});
});

// Import users into app
router.post('/import', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adduser');
    let [departments] = await db.query('SELECT * FROM departments');
    let [roles] = await db.query('SELECT * FROM roles WHERE power < 9999'); // Do not show SA
    try {
        let form = await appUtil.formidablePromise(req);
        let csvFile = form.files.importedusers;
        log.debug(csvFile);
        log.info(`Received '${csvFile.name}' with type ${csvFile.type} of size ${csvFile.size} at ${csvFile.path}`);

        // Validate that we have a CSV file
        // let success = true;
        // let error = '';
        success = false;
        error = 'File uploaded is not a CSV file';
        let filetype = csvFile.type;
        log.info('csvFile.type = ' + filetype)
        if (csvFile.size <= 0) {
            success = false;
            error = 'Invalid File';
            log.info('csvFile.size <= 0 TRUE')
        }
        if (csvFile.type === 'text/csv' || csvFile.type === 'application/vnd.ms-excel') {
            success = true;
            error = '';
        }

        if (!success) {
            res.render('users/importuser', {...renderConf, title: 'Import User', errmsg: error, departments: departments, roles: roles});
            return;
        }

        // Process CSV file (try to, final parsing)
        let parsedCSV = csv.parseFile(csvFile.path, {headers: ["username", "password", "fullname", "emailaddress", "role", "department"], renameHeaders: true, ignoreEmpty: true, strictColumnHandling: true});
        parsedCSV.on('error', error => {
            log.info(error);
            res.render('users/importuser', {...renderConf, title: 'Import User', errmsg: 'An error occurred parsing this CSV file', departments: departments, roles: roles});
        });
        let streamData = [];
        let errData = [], userData = [];
        parsedCSV.on('data', row => streamData.push(row));
        parsedCSV.on('end', async rowCount => {
            // Process End, further processing
            log.info(`Parsed ${rowCount} rows`);
            for (let row of streamData) {
                // Parse role and dept
                let role = (await db.query('SELECT id, name FROM roles WHERE `key` = ? AND power < 9999 LIMIT 1', [row.role.toLowerCase()]))[0][0];
                let dept = (await db.query('SELECT id, fullname FROM departments WHERE shortname = ? LIMIT 1', [row.department]))[0][0];
                if (!role || !dept) {
                    log.info(`Error processing row for ${row.username} (invalid ${(!role) ? "role" : "department"})`);
                    row.errorAt = (!role) ? "Invalid Role" : "Invalid Department";
                    errData.push(row);
                    continue;
                }
                log.info(`${row.username} - ${role.name} - ${dept.fullname}`);
                row.roleid = role.id;
                row.deptid = dept.id;
                userData.push(row);
            }
            log.debug(userData);
            log.info(`Success ${userData.length} records, Failed ${errData.length} rows`)
            if (userData.length <= 0) res.render('users/importuser', {...renderConf, title: 'Import User', errmsg: 'No user accounts found in file', departments: departments, roles: roles});
            else {
                // Place data on redis with a TTL to expire and generate a random id for me
                let rediskey = `importuser:${new Date().getTime()}:${auth.getSalt()}`;
                await cache.set(rediskey, JSON.stringify(userData), 'EX', 10800); // Set redis for 3 hours
                res.render('users/importusercheck', {...renderConf, title: 'Confirm Import User', errData: errData, dataCache: rediskey, userlen: userData.length});
            }
        });
    } catch (err) {
        log.error(err);
        res.render('users/importuser', {...renderConf, title: 'Import User', errmsg: "An error occurred uploading file", departments: departments, roles: roles});
    }
});

async function bulkAddUsers(redisKey, cachekey) {
    await cache.set(cachekey, JSON.stringify({status: "Getting data from cache", finished: false}), 'EX', 10800);
    let data = await cache.get(redisKey);
    data = JSON.parse(data);
    await cache.set(cachekey, JSON.stringify({status: `Preparing to add ${data.length} users`, finshed: false}), 'EX', 10800);
    let error = 0, cnt = 0;
    let errRow = [];
    for (let d of data) {
        cnt++;
        await cache.set(cachekey, JSON.stringify({status: `Processing ${cnt}/${data.length} users. Errors: ${error}`, finished: false}), 'EX', 10800);
        try {
            await auth.createUser(d.username, d.password, d.fullname, d.emailaddress, d.roleid, d.deptid);
        } catch (err) {
            log.error(err);
            error++;
            errRow.push(d.username);
        }
    }
    await cache.set(cachekey, JSON.stringify({status: `Finished importing ${cnt} users! There are ${error} errors. Redirecting you back shortly...`, finished: true, errors: errRow}), 'EX', 10800);
}

// Confirm that we are importing users into the app
router.post('/importConfirm', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adduser');
    let redisKey = req.body.cachekey;
    let newrediskey = `processimportU-${auth.getSalt()}`;
    await cache.set(newrediskey, JSON.stringify({status: "Processing", finished: false}), 'EX', 10800);
    bulkAddUsers(redisKey, newrediskey).then(() => log.info("Bulk add completed"));
    res.render('users/processImport', {...renderConf, title: 'Processing Import...', processKey: newrediskey});
});

// Check import users status
router.get('/importStatus/:key', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'adduser');
    let info = await cache.get(req.params.key);
    res.json(JSON.parse(info));
});

// List users
router.get('/list', async (req, res) => {
    let sucmsg = ""
    if (req.query.success && parseInt(req.query.success)) {
        let getusernameSQL = "SELECT fullname, username FROM users WHERE id=?";
        let [username] = await db.query(getusernameSQL, [req.query.success]);
        sucmsg = `User ${username[0].fullname} has been updated successfully!`;
    } else if (req.query.addsuc) {
        let getusernameSQL = "SELECT fullname, username FROM users WHERE username=?";
        let [username] = await db.query(getusernameSQL, [req.query.addsuc]);
        sucmsg = `${username[0].fullname} (${username[0].username}) has been added into the system!`
    }
    res.render('users/listuser', {...renderConf, title: 'User List', sucmsg: sucmsg});
});

// Get data of all users
router.get('/userdata', async (req, res) => {
    let sess = req.session;
    let power = sess.rolePower;
    let [users] = await db.query('SELECT u.*, r.name as rolename, d.shortname, d.fullname as deptname FROM users u JOIN departments d on u.department = d.id JOIN roles r on r.id = u.role WHERE u.role IN (SELECT id FROM roles WHERE power <= ?)', [power]);
    let data = [];
    for (let u of users) {
        log.debug(u);
        data.push({id: u.id, username: u.username, password: '*******', fullname: u.fullname, email: u.email, role: u.rolename, deptid: u.shortname, disabled: (u.disabled) ? "Disabled" : "Enabled", disint: u.disabled});
    }
    res.json({data: data});
});

// Edit user page
router.get('/edit/:userid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'edituser');
    let [departments] = await db.query('SELECT * FROM departments');
    let [roles] = await db.query('SELECT * FROM roles WHERE power < 9999'); // Do not show SA

    let getUserListSQL = 'SELECT * FROM users WHERE id=?';
    let user = (await db.query(getUserListSQL, [req.params.userid]))[0][0];
    delete user.password;
    delete user.salt; // Don't pass this to the user
    log.debug(user);
    res.render('users/edituser', {...renderConf, title: `Editing ${user.username}`, user: user, editUserFullName: user.fullname, departments: departments, roles: roles})
});

// Edit a user
router.post('/edit/:userid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'edituser');
    log.debug(req.body);
    log.info(`Has Password: ${req.body.password ? "true":"false"}`);
    log.info(`Is Disabled: ${req.body.disablestate ? "true" : "false"}`);
    let salt = auth.getSalt();
    let pw = (req.body.password) ? await auth.getHashedPw(req.body.password, salt) : null;
    let query = `UPDATE users SET fullname=?, email=?, role=?, department=?, disabled=? ${(pw) ? ", password=?, salt=? " : " "}WHERE id=?`;
    let data = [req.body.fullName, req.body.email, parseInt(req.body.userrole), parseInt(req.body.department), (req.body.disablestate) ? 1 : 0];
    if (pw) {
        data.push(pw, salt);
    }
    data.push(req.params.userid);
    await db.query(query, data);
    res.redirect(`/users/list?success=${req.params.userid}`);
});

// Enable/Disable a user
router.patch('/toggledisable/:userid', async (req, res) => {
    auth.hasPermission(req, res, renderConf, 'disableuser');
    let disableUserSQL = 'UPDATE users SET disabled= IF(disabled, 0, 1) WHERE id=?';
    await db.query(disableUserSQL, [req.params.userid]);
    let disableSQL = 'SELECT disabled, fullname FROM users WHERE id=?';
    let disabledState = (await db.query(disableSQL, [req.params.userid]))[0][0];
    res.json({userid: req.params.userid, disabled: disabledState.disabled, name: disabledState.fullname});
});

module.exports = router;
