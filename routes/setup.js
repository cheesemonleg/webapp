const express = require('express');
const router = express.Router();
const config = require('../config');
const log = require('../lib/logger');

// Test MySQL and Redis working
const db = require('../lib/db');
const cache = require('../lib/redis');
const storage = require('../lib/storage');
const mysql = require('mysql2');
const fs = require('fs');
const path = require('path');
const auth = require('../lib/auth');
const appUtil = require('../lib/util');

const renderConf = {appName: 'Setup AASP', apprev: appUtil.getCommitRev(), appver: appUtil.getVersion(), appenv: appUtil.getEnv()};

router.use('*', (req, res, next) => {
  if (process.env.SETUP) next();
  else res.render('setup/notauth', {title: 'SETUP DISABLED', appName: 'AASP', apprev: appUtil.getCommitRev(), appver: appUtil.getVersion(), appenv: appUtil.getEnv()});
});

// Main setup page
router.get('/', (req, res) => {
  res.render('setup/setupdb', {...renderConf, title: 'DB', conf: config });
});

function setCache(phase, message, finished) {
  cache.set('SETUPP', phase);
  cache.set('SETUPM', message);
  cache.set('SETUPF', finished);
  log.info(`[SETUP] ${phase}: ${message}`);
}

async function getCache() {
  return {phase: await cache.get('SETUPP'), msg: await cache.get('SETUPM'), fin: await cache.get('SETUPF')};
}

function deleteCache() {
  cache.del('SETUPP');
  cache.del('SETUPM');
  cache.del('SETUPF');
}

async function delStorage(bucketName) {
  return new Promise(async (res, rej) => {
    if (await storage.bucketExists(bucketName)) {
      let tmplist = [];
      let stream = await storage.listObjectsV2(bucketName, '', true);
      stream.on('data', (obj) => tmplist.push(obj.name));
      stream.on('error', (err) => rej(err));
      stream.on('end', async () => {
        await storage.removeObjects(bucketName, tmplist);
        await storage.removeBucket(bucketName);
        res();
      });
    } else {
      res(); // Bucket does not exist
    }
  });
}

async function setupStorage() {
  setCache('PREPARING', 'Setting up Storage connection', false);
  setCache('RUNNING', 'Removing existing buckets', false);

  log.info("Removing Secret Storage");
  await delStorage(config.storageConfig.bucketName);
  log.info("Removing Public Storage");
  await delStorage(config.storageConfig.bucketNamePub);
  log.info("Removed storage done!")

  setCache('RUNNING', 'Creating buckets', false);
  await storage.makeBucket(config.storageConfig.bucketName, config.storageConfig.bucketRegion);
  await storage.makeBucket(config.storageConfig.bucketNamePub, config.storageConfig.bucketRegion);

  setCache('RUNNING', 'Setting permissions on public bucket', false);
  await storage.setBucketPolicy(config.storageConfig.bucketNamePub, JSON.stringify(storage.pubPolicyVar));
}

async function setupDB() {
  await setupStorage();
  setCache('PREPARING', 'Obtaining MySQL Connection', false);
  let conn = mysql.createConnection({
    host: config.dbConfig.host,
    user: config.dbConfig.user,
    port: config.dbConfig.port,
    password: config.dbConfig.password,
    database: config.dbConfig.database,
    multipleStatements: true
  });

  const struc = fs.readFileSync(path.join(__dirname, '../dbschema/table.sql')).toString();
  const dbdata = fs.readFileSync(path.join(__dirname, '../dbschema/data.sql')).toString();

  try {
    conn.query(struc, (err) => {
      if (err) throw err
      setCache('RUNNING', 'Populating Default data', false);
      conn.query(dbdata, (err2) =>{
        if (err2) throw err2;
        setCache('FINISHED', 'Database Setup Complete. Press the Continue button to continue', true);
        conn.end();
      });
    });
  } catch (e) {
    log.info(e);
    setCache('ERROR', `Error Occurred: ${e.message}`, true);
  }
}

// Progress page of setup
router.get('/continue', async (req, res) => {
  res.render('setup/processing', {...renderConf, title: 'Creating DB'});
  setupDB().then(() => {log.info('Finished Setting up DB')});
});

// Check status of setup
router.get('/status', async (req, res) => {
  let info = await getCache();
  res.json(info);
});

// Create new admin account page
router.get('/createadmin', async (req, res) => {
  let [departments] = await db.query('SELECT * FROM departments');
  res.render('setup/adminuser', {...renderConf, title: 'Create Admin User', departments: departments});
});

// Add a new admin user to the webapp
router.post('/addadmin', async (req, res) => {
  try {
    let getRoleIdSQL = 'SELECT r.id FROM roles r WHERE r.key=? LIMIT 1';
    let [role] = await db.query(getRoleIdSQL, [req.body.role]);
    role = role[0].id;
    log.info(`RoleID: ${role} (${req.body.role})`);
    await auth.createUser(req.body.username, req.body.password, req.body.fullName, req.body.email, role, req.body.department)
    deleteCache();
    res.render('setup/complete', {...renderConf, title: 'Setup Complete!'});
  } catch (err) {
    log.error(err);
  }
});

module.exports = router;
